# frozen_string_literal: true

module Strum
  module Pipeline
    VERSION = "0.1.2"
  end
end
