# frozen_string_literal: true

require "strum/pipeline/version"
require "redis/objects"
require 'connection_pool'
require "strum/esb"
require "strum/pipeline/storage"
require "dry/configurable"
require "securerandom"

module Strum
  module Pipeline # rubocop:disable Style/Documentation
    extend Dry::Configurable

    setting :ttl, 15 * 60 # default ttl is 15 mins
    setting :redis_connection, {
      host: ENV.fetch("PIPELINE_REDIS_HOST", "localhost"),
      port: ENV.fetch("PIPELINE_REDIS_PORT", "6379"),
      db: ENV.fetch("PIPELINE_REDIS_DB", "0")
    }
    # setting :redis_connection_pool

    Strum::Esb.config.after_fork_hooks << proc do
      Redis::Objects.redis = ConnectionPool.new(size: 5, timeout: 5) do
        Redis.new(Strum::Pipeline.config.redis_connection)
      end
    end

    class Error < StandardError; end

    # Classes = []

    def self.included(base)
      base.class_eval do
        include Strum::Esb::Handler

        def after_headers_hook
          Thread.current[:pipeline] ||= self.class.pipeline_name
          Thread.current[:pipeline_id] ||= SecureRandom.hex(10)
        end
      end

      base.extend ClassMethods
      # Classes << base if base.is_a? Class
    end

    # class methods
    module ClassMethods
      def ttl(interval)
        Strum::Pipeline.config.ttl = interval
      end

      def init(message_type, message_binding, handler = nil)
        bind_to pipeline_name, message_type, message_binding, handler
      end

      def step(message_type, message_binding, handler = nil)
        bindings ||= (queue_opts && queue_opts[:bindings]) || {}
        bindings[:pipeline] ||= {}
        bindings[:pipeline][:name] = pipeline_name
        bindings[:pipeline][message_type] ||= []
        bindings[:pipeline][message_type] << message_binding
        from_queue pipeline_name, bindings: bindings
        _, *msg = Strum::Esb::Functions.public_send("#{message_type}_explain", message_binding)
        @handlers ||= {}
        params = msg.map{ |param| param&.to_s.gsub(/[^a-zA-Z0-9]/, "_")&.downcase }
        handler_key = ([message_type] + params).join("-")
        @handlers[handler_key] = handler.to_s if handler
      end

      def pipeline_name
        word = name.dup
        word.gsub!(/::/, "/")
        word.gsub!(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
        word.gsub!(/([a-z\d])([A-Z])/, '\1_\2')
        word.tr!("-", "_")
        word.downcase!
        word
      end
    end

    def storage
      @storage ||= Strum::Pipeline::Storage.new(pipeline_key, Strum::Pipeline.config.ttl)
    end

    def pipeline_key
      [Thread.current[:pipeline], Thread.current[:pipeline_id]].join("-")
    end
  end
end
