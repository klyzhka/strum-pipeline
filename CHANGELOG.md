
# Changelog
All notable changes to this project will be documented in this file.

## [0.1.2] - 2021-07-01
### Added
- Redis connection using env var or config by [@serhiy.nazarov].

## [0.1.1] - 2021-06-02
### Fixed
- custom handlers for resource name with `-` by [@anton.klyzhka].

### Changed
- `strum-esb` dependency version to `~> 0.1.1` by [@anton.klyzhka].
